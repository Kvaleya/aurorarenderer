﻿namespace AuroraRenderer
{
	class Program
	{
		static void Main(string[] args)
		{
			ViewWindow window = new ViewWindow(1280, 720);
			window.Run();
		}
	}
}
