﻿using OpenTK;

namespace AuroraRenderer
{
	class WorldEnvironment
	{
		public Vector3 SunIrradianceAtmosphereColored
		{
			get { return _sunColor * SunIrradianceTopOfAtmosphere; }
		}

		Vector3 _sunColor = new Vector3(1.0f, 0.99f, 0.975f);
		const float SunIrradianceTopOfAtmosphere = 1361;
		public Vector3 SunlightDirection = new Vector3(1, -0.5f, 0.2f).Normalized();

		public float Exposure = 1;

		public Vector3 CameraPosition;
		public Matrix4 CameraMatrix;
		public Matrix4 ProjectionMatrix;

		public ushort AuroraDebugLayerView = 0;

		public bool EnableTonemap = true;
		public bool EnableAuroraDebug = false;
	}
}
