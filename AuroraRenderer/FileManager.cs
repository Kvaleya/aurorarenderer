﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuroraRenderer
{
	public class FileManager : Glob.IFileManagerGlob
	{
		string _baseRelativePath;

		// Needed for Glob
		string _shaderSourcePath;
		string _shaderResolvedPath;
		string _shaderCompilePath;

		readonly TextOutput _textOutput;

		public FileManager(TextOutput textOutput, string baseRelativePath, string shaderSourcePath, string shaderResolvedPath, string shaderCompiledPath)
		{
			_textOutput = textOutput;
			_baseRelativePath = baseRelativePath;
			_shaderSourcePath = shaderSourcePath;
			_shaderResolvedPath = shaderResolvedPath;
			_shaderCompilePath = shaderCompiledPath;
		}

		string GetPathString(string path)
		{
			path = path.ToLowerInvariant();
			path = path.Replace('\\', '/');
			path = path.Replace("//", "/");
			path.Trim();
			return path;
		}
		
		public bool FileExists(string file)
		{
			string path = GetPathString(file);
			string relativePath = Path.Combine(_baseRelativePath, path);
			return File.Exists(relativePath);
		}

		public Stream GetStream(string file)
		{
			string path = GetPathString(file);

			string relativePath = Path.Combine(_baseRelativePath, path);

			if(File.Exists(relativePath))
			{
				try
				{
					return new FileStream(relativePath, FileMode.Open, FileAccess.Read);
				}
				catch(Exception e)
				{
					_textOutput.PrintException(OutputType.Error, "Error opening file: " + file, e);
					return null;
				}
			}

			_textOutput.Print(OutputType.Warning, "File not found: " + file);

			return null;
		}

		// Needed for Glob
		public string GetPathShaderSource(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderSourcePath;
			return Path.Combine(_shaderSourcePath, file);
		}

		public string GetPathShaderCacheResolved(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderResolvedPath;
			return Path.Combine(_shaderResolvedPath, file);
		}

		public string GetPathShaderCacheCompiled(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderCompilePath;
			return Path.Combine(_shaderCompilePath, file);
		}
	}
}
