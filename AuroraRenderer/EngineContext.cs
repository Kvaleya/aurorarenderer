﻿using System;
using Glob;

namespace AuroraRenderer
{
	class EngineContext
	{
		public KeyboardInputState Keyboard;
		public MouseInputState Mouse;
		public Device Device;

		public TextOutput TextOutput;
		public FileManager FileManager;

		public double Ellapsed;
		public double DeltaT;

		public int DisplayWidth, DisplayHeight;

		public WorldEnvironment Environment = new WorldEnvironment();
	}
}
