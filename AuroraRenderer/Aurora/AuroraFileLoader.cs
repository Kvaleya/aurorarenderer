﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace AuroraRenderer
{
	/// <summary>
	/// Loads aurora files and passes them to AuroraModule. Can detect changes in files and reload them at runtime.
	/// </summary>
	class AuroraFileLoader
	{
		const string AuroraDir = "Auroras";

		AuroraModule _module;
		FileSystemWatcher _watcher;
		EngineContext _engineContext;
		bool _doReload = false;
		Object _sync = new Object();

		public AuroraFileLoader(EngineContext engineContext, AuroraModule auroraModule)
		{
			_engineContext = engineContext;
			_module = auroraModule;
		}

		public void StartFileWatcher()
		{
			_watcher = new FileSystemWatcher();
			_watcher.Path = Path.Combine(System.Environment.CurrentDirectory, AuroraDir);
			_watcher.Filter = "*.xml";
			_watcher.Changed += (sender, args) =>
			{
				lock(_sync)
				{
					_doReload = true;
				}
			};

			_watcher.EnableRaisingEvents = true;
		}

		/// <summary>
		/// Reloads aurora files at runtime if changes are detected
		/// </summary>
		public void Update()
		{
			if(_doReload)
			{
				lock(_sync)
				{
					_doReload = false;
					// Watcher may detect file change before the text editor is done writing it
					Thread.Sleep(10);
					LoadAuroraFiles();
				}
			}
		}

		public void LoadAuroraFiles()
		{
			List<AuroraCurtain> curtains = null;
			List<AuroraSpiral> newSpirals = null;

			// When editing aurora files with Notepad++, sometimes the file modification date is changed before the changes are actually saved. Loading these files during this time fails, so simply try again later.
			for(int i = 0; i < 20; i++)
			{
				try
				{
					newSpirals = AuroraSpiral.LoadFromFile(_engineContext.FileManager, Path.Combine(AuroraDir, "auroraSpirals.xml"));
					curtains = AuroraCurtain.LoadFromFile(_engineContext.FileManager, Path.Combine(AuroraDir, "auroraCurtains.xml"));
					break;
				}
				catch(ArgumentNullException e)
				{
					Thread.Sleep(10);
					curtains = null;
				}
				catch(Exception e)
				{
					_engineContext.TextOutput.PrintException(OutputType.Error, "Error loading auroras", e);
					curtains = null;
					newSpirals = null;
				}
			}

			if(newSpirals != null)
			{
				_module.SetAuroraSpirlas(newSpirals);
			}

			if(curtains != null)
			{
				_module.SetAuroraCurtains(curtains[0], curtains[1], curtains[2], curtains[3]);
			}
		}
	}
}
