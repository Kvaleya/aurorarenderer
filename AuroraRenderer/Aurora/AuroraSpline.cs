﻿using System;
using OpenTK;

namespace AuroraRenderer
{
	/// <summary>
	/// Data needed to render one aurora spline
	/// </summary>
	class AuroraSpline
	{
		public int ElementCount = 0;
		public int BaseVertex = 0;
		public Vector2 Offset = Vector2.Zero;
		public int TexLayer = 0;
		public Vector4 PrimaryColor;
		public Vector4 SecondaryColor;
		public float TexCoordScale;

		public AuroraSpline(Tuple<int, int> draw, Vector2 offset, int texLayer, Vector4 primaryColor, Vector4 secondaryColor, float texCoordScale)
		{
			ElementCount = draw.Item2;
			BaseVertex = draw.Item1;
			Offset = offset;
			TexLayer = texLayer;
			PrimaryColor = primaryColor;
			SecondaryColor = secondaryColor;
			TexCoordScale = texCoordScale;
		}
	}
}
