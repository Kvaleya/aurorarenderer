﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using OpenTK;

namespace AuroraRenderer
{
	/// <summary>
	/// Describes the large-scale shape and animation of the aurora, as well as its curtain type (color) and small-scale detail shape (fluid simulation layer)
	/// </summary>
	[Serializable]
	class AuroraSpiral
	{
		public int SplineCount = 1;

		// Number of vertices between neighbour control points in a spline
		// Defines the smoothness of bends in the spline shape
		public int SplineInnerVertices = 4;
		public int SplineControlPoints = 50;
		// Number of points between two splines that are not used
		public int GapPoints = 0;
		public float Radius = 1f;
		// The radius from which the spiral starts
		public float StartRadius = 0f;
		public float InitialAngle = 0f;
		// Controls how much the spiral curls (0 = straight line)
		public float AngleMultiplier = 50f;
		public float RandomOffset = 0.0f;
		public float RandomOffsetMultiplier = 0.0f;
		// Random movement period
		public float RandomnessPeriod = 1f;
		// Controls wow densely the fluid sim details are mapped into the spiral
		public float TexCoordScale = 1.0f;
		public float SplineWidth = 0.05f;

		// The array slice of fluid simulation to use - defines the small-scale shape of the aurora
		public int SplineType = 0;

		// Each component of color maps to one type of aurora curtain
		public Vector4 PrimaryColor = new Vector4(0, 1, 0, 0);
		public Vector4 SecondaryColor = new Vector4(1, 0, 0, 0);

		/// <summary>
		/// Generates aurora splines from spiral parameters
		/// </summary>
		/// <param name="splineGen">Spline generator instance</param>
		/// <param name="randomness">Random values used for spline animation</param>
		/// <param name="ellapsed">Time ellapsed since program start</param>
		/// <param name="randomnessFirstIndex">Offset into randomness array, used to make different spirals that use same randomness array look different</param>
		/// <returns>List of aurora spline objects that contain all data needed to render the splines.</returns>
		public List<AuroraSpline> GenSplines(SplineGen splineGen, float[] randomness, double ellapsed, int randomnessFirstIndex)
		{
			List<Vector2> polygon = new List<Vector2>();

			List<AuroraSpline> splines = new List<AuroraSpline>();

			int totalPoints = SplineCount * (SplineControlPoints + GapPoints);

			for(int i = 0; i < totalPoints; i++)
			{
				float t = i / (float)(totalPoints - 1);
				var angle = InitialAngle + t * AngleMultiplier;

				var sin = (float)Math.Sin(angle);
				var cos = (float)Math.Cos(angle);

				float localRadius = StartRadius + t * (Radius - StartRadius);

				double ellpasedForRandom = ellapsed / RandomnessPeriod;

				var random = GetRandomOffset(randomness, i * 11 + randomnessFirstIndex, 8, t + (float)(ellpasedForRandom - Math.Floor(ellpasedForRandom)), 0.7);

				localRadius += (RandomOffset + RandomOffsetMultiplier * t) * random;

				Vector2 point = new Vector2(sin, cos) * Math.Max(localRadius, 0);

				polygon.Add(point);

				if(polygon.Count == SplineControlPoints)
				{
					splines.Add(new AuroraSpline(splineGen.AddSpline(SplineInnerVertices, SplineWidth, polygon.ToArray()), new Vector2(0.5f), SplineType, PrimaryColor, SecondaryColor, TexCoordScale));
					polygon.Clear();
					i += GapPoints;
				}
			}

			return splines;
		}

		float GetRandomOffset(float[] randomness, int randomnessFirstIndex, int waves, float t, double power = 0.5)
		{
			double angle = t * 2 * Math.PI;

			double result = 0;
			double amplitude = power;

			for(int i = 0; i < waves; i++)
			{
				int index = (randomnessFirstIndex + i) % randomness.Length;
				double offset = randomness[index] * 2 * Math.PI;

				result += Math.Cos(offset + angle) * amplitude;

				amplitude *= power;
			}

			return (float)result;
		}

		public static List<AuroraSpiral> LoadFromFile(FileManager fileManager, string filename)
		{
			using(var stream = fileManager.GetStream(filename))
			{
				XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());
				DataContractSerializer ser = new DataContractSerializer(typeof(List<AuroraSpiral>));

				return (List<AuroraSpiral>)ser.ReadObject(reader, true);
			}
		}

		public void SaveToFile(string filename)
		{
			var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };

			DataContractSerializer ds = new DataContractSerializer(typeof(List<AuroraSpiral>));

			using(var writer = XmlWriter.Create(filename, settings))
			{
				ds.WriteObject(writer, new List<AuroraSpiral>()
					{
						this,
					});
			}
		}
	}
}
