﻿using System;
using System.Collections.Generic;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace AuroraRenderer
{
	class TonemapModule : RenderModule
	{
		GraphicsPipeline _psoTonemap;
		GraphicsPipeline _psoTonemapDisabled;

		public TonemapModule(EngineContext engineContext) : base(engineContext)
		{
			_psoTonemap = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("tonemap.frag"), null, new RasterizerState(), new DepthState());

			_psoTonemapDisabled = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("tonemap.frag", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NO_TONEMAP_GAMMA", ""),
			}), null, new RasterizerState(), new DepthState());
		}

		public void Tonemap(Texture2D sceneColorHdr, bool enabled = true)
		{
			if(enabled)
			{
				Device.BindPipeline(_psoTonemap);
			}
			else
			{
				Device.BindPipeline(_psoTonemapDisabled);
			}
			Device.BindTexture(sceneColorHdr, 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
