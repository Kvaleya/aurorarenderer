﻿using System.Collections.Generic;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace AuroraRenderer
{
	// This class contains high level frame rendering
	class WorldRenderer
	{
		EngineContext _engineContext;

		private Device Device { get { return _engineContext.Device; } }

		GraphicsPipeline _psoClear;
		GraphicsPipeline _psoAuroraBlur;

		AtmosphereModule _moduleAtmosphere;
		TonemapModule _moduleTonemap;
		AuroraModule _moduleAurora;
		StarModule _moduleStar;

		Texture2D _texSceneColor;
		Texture2D _texSceneDepth;
		Texture2D _texAuroras;
		FrameBuffer _fboScene;

		// Fences are inserted at the end of the frame
		int _maxFences = 16;
		// Keep the fences of last several frames
		// Other systems will rely on this (for uploading data to GPU using persistently mapped buffers)
		Queue<FenceSync> _fencesFrameEnd = new Queue<FenceSync>();

		long _frameNumber = 0;

		public WorldRenderer(EngineContext engineContext)
		{
			_engineContext = engineContext;

			_psoClear = new GraphicsPipeline(engineContext.Device, null, null, null, new RasterizerState(),
				new DepthState(DepthFunction.Always, true));
			_psoAuroraBlur = new GraphicsPipeline(engineContext.Device, _engineContext.Device.GetShader("fullscreenSimple.vert"), _engineContext.Device.GetShader("auroraBlur.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.Additive));
			// Additive blending: BlendingFactorSrc: One, BlendingFactorDest: One, BlendEquationMode: Add

			_moduleAtmosphere = new AtmosphereModule(engineContext);
			_moduleTonemap = new TonemapModule(engineContext);
			_moduleAurora = new AuroraModule(engineContext);
			_moduleStar = new StarModule(engineContext);

			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.StencilTest);
			GL.ClipControl(ClipOrigin.LowerLeft, ClipDepthMode.ZeroToOne);
		}

		public void OnResize(int renderWidth, int renderHeight)
		{
			Device.Invalidate();

			// Recreate all resolution-dependent textures
			_texSceneColor?.Dispose();
			_texSceneDepth?.Dispose();
			_texAuroras?.Dispose();
			_fboScene?.Dispose();

			_texSceneColor = new Texture2D(Device, "SceneColor", SizedInternalFormatGlob.RGBA16F, renderWidth, renderHeight, 1);
			_texSceneDepth = new Texture2D(Device, "SceneDepth", SizedInternalFormatGlob.DEPTH32F_STENCIL8, renderWidth, renderHeight, 1);

			const int auroraDivideX = 4;
			const int auroraDivideY = 4;

			_texAuroras = new Texture2D(Device, "SceneAurora", SizedInternalFormatGlob.RGBA16F, renderWidth / auroraDivideX, renderHeight / auroraDivideY);
			Glob.Utils.SetTextureParameters(_engineContext.Device, _texAuroras, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			_fboScene = new FrameBuffer();
			
			Device.BindFrameBuffer(_fboScene, FramebufferTarget.Framebuffer);
			_fboScene.Attach(FramebufferAttachment.ColorAttachment0, _texSceneColor);
			_fboScene.Attach(FramebufferAttachment.DepthStencilAttachment, _texSceneDepth);
		}

		public void RenderFrame()
		{
			var frameSync = new FenceSync(_frameNumber);
			Device.Invalidate();

			_moduleAurora.UploadPhase(frameSync);

			// Make uploads visible
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);

			// Bind the default framebuffer
			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

			Device.BindPipeline(_psoClear);

			GL.Viewport(0, 0, _engineContext.DisplayWidth, _engineContext.DisplayHeight);
			GL.ClearColor(0f, 0f, 0f, 1f);
			GL.ClearDepth(0);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			_moduleAurora.Update(_engineContext.Environment.EnableAuroraDebug);

			_moduleAurora.Render(_texAuroras, _engineContext.Environment.CameraMatrix, _engineContext.Environment.ProjectionMatrix);

			// Push viewport
			using(Device.BindFrameBufferPushViewport(_fboScene, "Scene rendering"))
			{
				_moduleAtmosphere.RenderAtmosphere(_engineContext.Environment.ProjectionMatrix, _engineContext.Environment.CameraMatrix, _engineContext.Environment.SunlightDirection, _engineContext.Environment.SunIrradianceAtmosphereColored, _engineContext.Environment.Exposure, _engineContext.Environment.CameraPosition.Y);

				_moduleStar.RenderStars(_moduleAtmosphere.LutTransmittance);

				// Make sure the aurora compute shader result is visible
				GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

				// Draw the aurora with a slight blur to hide undersampling
				_engineContext.Device.BindPipeline(_psoAuroraBlur);
				_engineContext.Device.BindTexture(_texAuroras, 0);
				_engineContext.Device.ShaderFragment.SetUniformF("texelSize", new Vector2(1f / _texAuroras.Width, 1f / _texAuroras.Height));
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			}
			// Pop viewport

			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

			_moduleTonemap.Tonemap(_texSceneColor, _engineContext.Environment.EnableTonemap);

			// Debug visualizations
			_moduleAurora.RenderDebugFluidTexture(_engineContext.Environment.AuroraDebugLayerView, _engineContext.DisplayWidth, _engineContext.DisplayHeight);

			if(_engineContext.Environment.EnableAuroraDebug)
			{
				_moduleAurora.RenderDebugSplineTexture(_engineContext.DisplayWidth, _engineContext.DisplayHeight);
			}

			// Frame end
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);
			// Fence sync to avoid overwriting a buffer section that is currently in use
			frameSync.Create();

			_fencesFrameEnd.Enqueue(frameSync);
			if(_fencesFrameEnd.Count > _maxFences)
			{
				var fenceDelete = _fencesFrameEnd.Dequeue();
				fenceDelete.Dispose();
			}

			_frameNumber++;
		}
	}
}
