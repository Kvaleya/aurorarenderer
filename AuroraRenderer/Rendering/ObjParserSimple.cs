﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace AuroraRenderer
{
	[StructLayout(LayoutKind.Sequential, Pack = 4)]
	struct Vertex
	{
		public const int Size = 12 + 8 + 12;

		public Vector3 Position;
		public Vector2 TexCoord;
		public Vector3 Normal;
	}

	class IntVec3
	{
		public readonly int X, Y, Z;

		public IntVec3(int x, int y, int z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		protected bool Equals(IntVec3 other)
		{
			return X == other.X && Y == other.Y && Z == other.Z;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(ReferenceEquals(this, obj)) return true;
			if(obj.GetType() != this.GetType()) return false;
			return Equals((IntVec3)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = X;
				hashCode = (hashCode * 397) ^ Y;
				hashCode = (hashCode * 397) ^ Z;
				return hashCode;
			}
		}
	}

	static class ObjParserSimple
	{
		// Simple obj parser
		// Only intended for loading the star mesh
		// Supports position, texcoords, normals and faces of 3 or more vertices
		// Does not support relative indices etc.
		public static bool TryParseObj(StreamReader sr, out List<Vertex> vertices, out List<int> indices)
		{
			List<Vector3> positions = new List<Vector3>();
			List<Vector3> normals = new List<Vector3>();
			List<Vector2> texcoords = new List<Vector2>();

			vertices = new List<Vertex>();
			Dictionary<IntVec3, int> vertexToIndexMap = new Dictionary<IntVec3, int>();

			indices = new List<int>();

			bool errors = false;

			while(!sr.EndOfStream)
			{
				var line = sr.ReadLine().Split(' ');

				if(line.Length == 0 || string.IsNullOrEmpty(line[0]))
					continue;

				if(line[0] == "v" && line.Length > 3)
				{
					float x = 0, y = 0, z = 0;
					bool success = true;
					success = success && float.TryParse(line[1], NumberStyles.Any, CultureInfo.InvariantCulture, out x);
					success = success && float.TryParse(line[2], NumberStyles.Any, CultureInfo.InvariantCulture, out y);
					success = success && float.TryParse(line[3], NumberStyles.Any, CultureInfo.InvariantCulture, out z);

					if(!success)
					{
						x = float.NaN;
						y = float.NaN;
						z = float.NaN;

						errors = true;
					}

					positions.Add(new Vector3(x, y, z));
				}

				if(line[0] == "vn" && line.Length > 3)
				{
					float x = 0, y = 0, z = 0;
					bool success = true;
					success = success && float.TryParse(line[1], NumberStyles.Any, CultureInfo.InvariantCulture, out x);
					success = success && float.TryParse(line[2], NumberStyles.Any, CultureInfo.InvariantCulture, out y);
					success = success && float.TryParse(line[3], NumberStyles.Any, CultureInfo.InvariantCulture, out z);

					if(!success)
					{
						x = float.NaN;
						y = float.NaN;
						z = float.NaN;

						errors = true;
					}

					normals.Add(new Vector3(x, y, z));
				}

				if(line[0] == "vt" && line.Length > 2)
				{
					float x = 0, y = 0;
					bool success = true;
					success = success && float.TryParse(line[1], NumberStyles.Any, CultureInfo.InvariantCulture, out x);
					success = success && float.TryParse(line[2], NumberStyles.Any, CultureInfo.InvariantCulture, out y);

					if(!success)
					{
						x = float.NaN;
						y = float.NaN;

						errors = true;
					}

					texcoords.Add(new Vector2(x, y));
				}

				if(line[0] == "f" && line.Length > 3)
				{
					List<int> faceIndices = new List<int>();

					for(int i = 1; i < line.Length; i++)
					{
						var faceLine = line[i].Split('/');
						if(faceLine.Length < 3)
						{
							errors = true;
							continue;
						}

						int p = -1, t = -1, n = -1;

						bool success = true;

						success = success && int.TryParse(faceLine[0], out p);
						success = success && int.TryParse(faceLine[1], out t);
						success = success && int.TryParse(faceLine[2], out n);

						IntVec3 v = new IntVec3(p, t, n);

						int index = 0;

						if(!success || p < 0 || t < 0 || n < 0)
						{
							errors = true;
							continue;
						}

						p--;
						t--;
						n--;

						if(vertexToIndexMap.ContainsKey(v))
						{
							index = vertexToIndexMap[v];
						}
						else
						{
							index = vertices.Count;
							vertices.Add(new Vertex()
							{
								Position = positions[p],
								TexCoord = texcoords[t],
								Normal = normals[n],
							});
						}

						// For polygons of more than 3 vertices
						if(faceIndices.Count > 2)
						{
							faceIndices.Add(faceIndices[0]);
							faceIndices.Add(faceIndices[faceIndices.Count - 2]);
						}

						faceIndices.Add(index);
					}

					indices.AddRange(faceIndices);
				}
			}

			return !errors;
		}
	}
}
