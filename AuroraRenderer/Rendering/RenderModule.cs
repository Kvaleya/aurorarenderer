﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;

namespace AuroraRenderer
{
	abstract class RenderModule
	{
		protected readonly EngineContext EngineContext;
		protected Device Device { get { return EngineContext.Device; } }

		public RenderModule(EngineContext engineContext)
		{
			EngineContext = engineContext;
		}
	}
}
