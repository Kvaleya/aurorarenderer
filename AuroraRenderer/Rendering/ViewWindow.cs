﻿using System;
using System.Drawing;
using System.Globalization;
using Glob;
using OpenTK;

namespace AuroraRenderer
{
	// Main window
	class ViewWindow : GameWindow
	{
		public Tuple<int, int> TopLeftPoint
		{
			get
			{
				Point point = this.PointToScreen(new System.Drawing.Point(0, 0));
				return new Tuple<int, int>(point.X, point.Y);
			}
		}

		const string WindowTitle = "AuroraRenderer";

		EngineContext _engineContext;

		WorldManager _worldManager;
		WorldRenderer _renderer;

		long _lastSecond = 0;
		int _frames = 0;

		public ViewWindow(int w, int h)
			: base(w, h)
		{
			var textOutput = TextOutput.CreateCommon();
			var fileManager = new FileManager(textOutput, "", "Shaders", "Shaders/Resolved", "Shaders/Compiled");

			var device = new Device(textOutput, fileManager);
			DebugMessageManager.SetupDebugCallback(textOutput);

			this.VSync = VSyncMode.Off;

			_engineContext = new EngineContext();
			_engineContext.Device = device;
			_engineContext.Keyboard = new KeyboardInputState();
			_engineContext.Mouse = new MouseInputState();

			_engineContext.TextOutput = textOutput;
			_engineContext.FileManager = fileManager;

			_engineContext.DisplayWidth = this.Width;
			_engineContext.DisplayHeight = this.Height;

			_worldManager = new WorldManager(_engineContext);

			_renderer = new WorldRenderer(_engineContext);
		}
		
		// We use the window as the main class of the program since it already contains a render loop
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			_engineContext.Device.Update();

			_worldManager.Update(this.Focused);

			_renderer.RenderFrame();
			
			SwapBuffers();

			double t = _engineContext.Ellapsed - _lastSecond;

			_frames++;

			if(t > 1)
			{
				//Simple FPS measurement
				_lastSecond = (long) Math.Floor(_engineContext.Ellapsed);
				double fps = Math.Round(_frames / t, 1);
				this.Title = WindowTitle + " FPS: " + fps.ToString(CultureInfo.InvariantCulture);
				_frames = 0;
			}
		}

		protected override void OnResize(EventArgs e)
		{
			int w = Math.Max(Width, 1);
			int h = Math.Max(Height, 1);
			_engineContext.DisplayWidth = w;
			_engineContext.DisplayHeight = h;
			_renderer?.OnResize(w, h);
		}
	}
}
