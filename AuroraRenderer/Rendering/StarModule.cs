﻿using System;
using System.Collections.Generic;
using System.IO;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace AuroraRenderer
{
	class StarModule : RenderModule
	{
		// OpenGL vertex and index buffer objects
		int _vbo, _ebo;

		int _indexCount;

		GraphicsPipeline _psoStarRender;
		VertexBufferSource _vertexBuffer;

		public StarModule(EngineContext engineContext)
			: base(engineContext)
		{
			List<Vertex> vertices = new List<Vertex>();
			List<int> indices = new List<int>();

			bool error = false;

			try
			{
				using(StreamReader sr = new StreamReader(engineContext.FileManager.GetStream("Meshes/starmesh_color_in_normals.obj")))
				{
					if(!ObjParserSimple.TryParseObj(sr, out vertices, out indices))
					{
						engineContext.TextOutput.Print(OutputType.Error, "Error loading star mesh");
					}
				}
			}
			catch(Exception e)
			{
				engineContext.TextOutput.Print(OutputType.Error, "Error loading star mesh: " + e.Message);
			}

			_vbo = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(Vertex.Size * vertices.Count),
					vertices.ToArray(), BufferStorageFlags.None, "StarVbo");
			_ebo = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(4 * indices.Count),
				indices.ToArray(), BufferStorageFlags.None, "StarEbo");

			_indexCount = indices.Count;

			_psoStarRender = new GraphicsPipeline(Device, Device.GetShader("stars.vert"), Device.GetShader("stars.frag"), new VertexBufferFormat(
				new VertexAttribDescription(0, 0, 3, 0, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(1, 0, 2, 12, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(2, 0, 3, 20, VertexAttribType.Float, VertexAttribClass.Float, false)
				), new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.Additive));

			_vertexBuffer = new VertexBufferSource(0, new VertexBufferBinding(_vbo, 0, Vertex.Size, 0));
		}

		public void RenderStars(Texture2D transmittanceLut)
		{
			var rotationMatrix = Matrix4.CreateFromQuaternion(Quaternion.FromAxisAngle(Vector3.UnitX, (float) (-85 / 180.0 * Math.PI)));

			Device.BindPipeline(_psoStarRender);
			Device.BindTexture(transmittanceLut, 0);
			Device.ShaderVertex.SetUniformF("world", rotationMatrix);
			Device.ShaderVertex.SetUniformF("camera", EngineContext.Environment.CameraMatrix);
			Device.ShaderVertex.SetUniformF("projection", EngineContext.Environment.ProjectionMatrix);
			Device.ShaderFragment.SetUniformF("exposure", EngineContext.Environment.Exposure);
			Device.BindVertexBufferSource(_vertexBuffer);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
			GL.DrawElements(PrimitiveType.Triangles, _indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}
	}
}
