﻿using System;
using System.Collections.Generic;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace AuroraRenderer
{
	// Based on:
	// Rendering Parametrizable Planetary Atmospheres with Multiple Scattering in Real-time
	// https://cgg.mff.cuni.cz/~oskar/research.html
	// https://cgg.mff.cuni.cz/~oskar/projects/CESCG2009/Elek2009.pdf
	// Physically Based Sky, Atmosphere & Cloud Rendering
	// https://www.ea.com/frostbite/news/physically-based-sky-atmosphere-and-cloud-rendering
	class AtmosphereModule : RenderModule
	{
		const int LutTransmittanceSizeX = 64;
		const int LutTransmittanceSizeY = 64;
		const int LutScatteringSizeX = 32;
		const int LutScatteringSizeY = 256;
		const int LutScatteringSizeZ = 32;

		const int NumSamplesTransmittance = 40;
		const int NumSamplesTransmittanceFast = 10;
		const int NumSamplesScattering = 20;
		const int NumSamplesMultipleScattering = 5;

		public Texture2D LutTransmittance { get { return _texLutTransmittance; } }

		GraphicsPipeline _psoAtmosphereRender;

		Atmosphere _atmoData;

		Texture2D _texLutTransmittance;
		Texture3D _texLutScatteringSum;
		Texture3D _texLutScatteringSumSwap;
		Texture3D _texLutScatteringMultiple;
		Texture3D _texLutScatteringMultipleSwap;

		ComputePipeline _psoGenerateLutTransmittance;
		ComputePipeline _psoGenerateLutSingleScattering;
		ComputePipeline _psoGenerateLutMultipleScattering;

		public AtmosphereModule(EngineContext engineContext) : base(engineContext)
		{
			_psoAtmosphereRender = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("atmo_render.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Equal, true));

			_atmoData = new Atmosphere();

			var definesTransmittance = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NUMSAMPLES_PRIMARY", NumSamplesScattering.ToString()),
				new Tuple<string, string>("NUMSAMPLES_SECONDARY", NumSamplesTransmittance.ToString()),
				new Tuple<string, string>("NUMSAMPLES_MULTIPLE", NumSamplesMultipleScattering.ToString()),
			};
			var definesScattering = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NUMSAMPLES_PRIMARY", NumSamplesScattering.ToString()),
				new Tuple<string, string>("NUMSAMPLES_SECONDARY", NumSamplesTransmittanceFast.ToString()),
				new Tuple<string, string>("NUMSAMPLES_MULTIPLE", NumSamplesMultipleScattering.ToString()),
			};

			_psoGenerateLutTransmittance = new ComputePipeline(Device,
				Device.GetShader("atmo_genTransmittanceLUT.comp", definesTransmittance));

			_psoGenerateLutSingleScattering = new ComputePipeline(Device,
				Device.GetShader("atmo_genSingleScatLUT.comp", definesScattering));

			_psoGenerateLutMultipleScattering = new ComputePipeline(Device,
				Device.GetShader("atmo_genMultipleScatLUT.comp", definesScattering));

			PrecomputeAtmosphereLuts(16);
		}

		public void PrecomputeAtmosphereLuts(int scatteringOrders)
		{
			_texLutTransmittance?.Dispose();
			_texLutScatteringSum?.Dispose();
			_texLutScatteringSumSwap?.Dispose();
			_texLutScatteringMultiple?.Dispose();
			_texLutScatteringMultipleSwap?.Dispose();

			// Create textures
			_texLutTransmittance = new Texture2D(Device, "AtmoTransmittanceLut", SizedInternalFormatGlob.RGBA16F, LutTransmittanceSizeX, LutTransmittanceSizeY, 1);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);

			_texLutScatteringSum = CreateScatteringTexture(Device, "AtmosphericScatteringSumA");
			_texLutScatteringSumSwap = CreateScatteringTexture(Device, "AtmosphericScatteringSumB");
			_texLutScatteringMultiple = CreateScatteringTexture(Device, "AtmosphericScatteringMultipleA");
			_texLutScatteringMultipleSwap = CreateScatteringTexture(Device, "AtmosphericScatteringMultipleB");

			// Precompute transmittance
			Device.BindPipeline(_psoGenerateLutTransmittance);
			SetAtmoUniforms(_psoGenerateLutTransmittance.ShaderCompute);
			_psoGenerateLutTransmittance.ShaderCompute.SetUniformI("lutSize", LutTransmittanceSizeX, LutTransmittanceSizeY);
			Device.BindImage2D(0, _texLutTransmittance, TextureAccess.WriteOnly);
			Device.DispatchComputeThreads(LutTransmittanceSizeX, LutTransmittanceSizeY);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Single scattering
			Device.BindPipeline(_psoGenerateLutSingleScattering);
			SetAtmoUniforms(_psoGenerateLutSingleScattering.ShaderCompute);
			_psoGenerateLutSingleScattering.ShaderCompute.SetUniformI("lutSize", LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);
			Device.BindImage3D(0, _texLutScatteringSum, TextureAccess.WriteOnly);
			Device.BindTexture(_texLutTransmittance, 0);
			Device.DispatchComputeThreads(LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			var multipleSource = _texLutScatteringSum;
			var multipleTarget = _texLutScatteringMultiple;
			var sumSource = _texLutScatteringSum;
			var sumTarget = _texLutScatteringSumSwap;

			// Multiple scattering
			for(int i = 1; i < scatteringOrders; i++)
			{
				Device.BindPipeline(_psoGenerateLutMultipleScattering);
				SetAtmoUniforms(_psoGenerateLutMultipleScattering.ShaderCompute);
				_psoGenerateLutMultipleScattering.ShaderCompute.SetUniformI("lutSize", LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);
				Device.BindImage3D(0, multipleTarget, TextureAccess.WriteOnly);
				Device.BindImage3D(1, sumSource, TextureAccess.ReadOnly);
				Device.BindImage3D(2, sumTarget, TextureAccess.WriteOnly);
				Device.BindTexture(multipleSource, 0);
				Device.DispatchComputeThreads(LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				Texture3D temp;

				if(i == 1)
				{
					multipleSource = _texLutScatteringMultiple;
					multipleTarget = _texLutScatteringMultipleSwap;
					sumSource = _texLutScatteringSumSwap;
					sumTarget = _texLutScatteringSum;
				}
				else
				{
					temp = multipleSource;
					multipleSource = multipleTarget;
					multipleTarget = temp;

					temp = sumSource;
					sumSource = sumTarget;
					sumTarget = temp;
				}

				temp = _texLutScatteringSum;
				_texLutScatteringSum = _texLutScatteringSumSwap;
				_texLutScatteringSumSwap = temp;

			}

			_texLutScatteringSumSwap?.Dispose();
			_texLutScatteringMultiple?.Dispose();
			_texLutScatteringMultipleSwap?.Dispose();
		}

		Texture3D CreateScatteringTexture(Device device, string name)
		{
			var tex = new Texture3D(device, name, SizedInternalFormatGlob.RGBA32F, LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ, 1);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
			return tex;
		}

		public void RenderAtmosphere(Matrix4 projectionMatrix, Matrix4 cameraMatrix, Vector3 sunDir, Vector3 sunColor, float exposure, float cameraHeight)
		{
			Vector4 ray00, ray01, ray11, ray10;
			Utils.GetCameraCornerRays(projectionMatrix, cameraMatrix, out ray00, out ray10, out ray11, out ray01);

			Device.BindPipeline(_psoAtmosphereRender);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray00", ray00);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray01", ray01);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray10", ray10);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray11", ray11);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("sunDir", sunDir);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("sunColor", sunColor);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("exposure", exposure);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("cameraHeight", cameraHeight * 1000);
			SetAtmoUniforms(_psoAtmosphereRender.ShaderFragment);

			Device.BindTexture(_texLutScatteringSum, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}

		void SetAtmoUniforms(Shader shader)
		{
			if(_atmoData == null)
				return;

			shader.SetUniformF("atmo_planet_radius", _atmoData.PlanetRadius);
			shader.SetUniformF("atmo_radius", _atmoData.Radius);

			shader.SetUniformF("atmo_coef_rayleigh", _atmoData.CoefRayleigh);
			shader.SetUniformF("atmo_coef_ozone", _atmoData.CoefOzone);

			shader.SetUniformF("atmo_coef_mie", _atmoData.CoefMie);
			shader.SetUniformF("atmo_g_mie", _atmoData.GMie);
			shader.SetUniformF("atmo_a_mie", _atmoData.AMie);

			shader.SetUniformF("atmo_h0_R", _atmoData.H0Rayleigh);
			shader.SetUniformF("atmo_h0_M", _atmoData.H0Mie);

			shader.SetUniformF("atmo_recover_mie", _atmoData.AtmoRecoverMie);
		}

		public Vector3 GetOutScattering(Vector3 direction)
		{
			return _atmoData.GetOutScattering(direction);
		}
	}

	class Atmosphere
	{
		public float PlanetRadius = 6378000.00f;
		public float Radius = 100000.00f;

		// RGB, where the wavelengths are 680, 550 and 440nm.
		public Vector3 CoefRayleigh = new Vector3(5.80000E-6f, 0.00001f, 0.00003f);
		public Vector3 CoefOzone = new Vector3(2.05560E-6f, 4.97880E-6f, 2.13600E-7f) * 1.5f;

		public float CoefMie = 2.00000E-6f;
		public float GMie = -0.85f;
		public float AMie = 1.11f;

		public float H0Rayleigh = 8000.00f;
		public float H0Mie = 1200.00f;

		public Vector3 AtmoRecoverMie
		{
			get
			{
				double red = CoefRayleigh.X;
				return new Vector3((float)(red / CoefRayleigh.X), (float)(red / CoefRayleigh.Y),
					(float)(red / CoefRayleigh.Z));
			}
		}

		static float PhaseFuncRayleigh(float cosTheta)
		{
			return 0.75f * (1.0f + cosTheta * cosTheta);
		}

		static float PhaseFuncMie(float cosTheta, float g)
		{
			float g2 = g * g;
			float nom = 3.0f * (1.0f - g2) * (1.0f + cosTheta * cosTheta);
			float denom = 2.0f * (2.0f + g2) * (float)Math.Pow(1.0 + g2 - 2.0 * g * cosTheta, 1.5);
			return nom / denom;
		}

		Vector2 Density(Vector3 p)
		{
			float h = (p - new Vector3(0, -PlanetRadius, 0)).Length - PlanetRadius;
			return new Vector2((float)Math.Exp(-h / H0Rayleigh), (float)Math.Exp(-h / H0Mie));
		}

		Vector2 Transmittance(Vector3 pa, Vector3 pb)
		{
			const int samples = 5;
			//const float pi4 = 12.566370614359172953850573533118f;
			Vector3 add = (pb - pa) / samples;
			Vector2 temp = new Vector2(0);

			Vector3 p = pa;
			for(int i = 0; i < samples; i++)
			{
				p = pa + add * (i + 0.5f);
				temp += Density(p);
			}

			return temp * add.Length;
		}

		static Vector2 RaySphereIntersection(Vector3 center, float radius, Vector3 origin, Vector3 direction, float depth)
		{
			float r0 = 0;
			float r1 = 0;

			Vector3 distvec = (origin - center);
			float dirdotdist = Vector3.Dot(direction, distvec);
			float temp = dirdotdist * dirdotdist - distvec.LengthSquared + radius * radius;

			if(temp > 0)
			{
				temp = (float)Math.Sqrt(temp);
				r0 = -dirdotdist - temp;
				r1 = -dirdotdist + temp;
			}

			r0 = Math.Max(r0, 0.0f);
			r1 = Math.Min(Math.Max(r1, 0.0f), depth);

			return new Vector2(r0, r1);
		}

		public Vector3 GetOutScattering(Vector3 dir)
		{
			Vector3 p = new Vector3(0);

			Vector2 r_atmo = RaySphereIntersection(new Vector3(0, -PlanetRadius, 0), PlanetRadius + Radius, p, dir, 1000000);

			Vector3 pc = p + dir * r_atmo.Y;

			Vector2 scat = Transmittance(p, pc);

			Vector3 outscat = new Vector3(1);

			outscat.X *= (float)Math.Exp(-scat.X * CoefRayleigh.X);
			outscat.Y *= (float)Math.Exp(-scat.X * CoefRayleigh.Y);
			outscat.Z *= (float)Math.Exp(-scat.X * CoefRayleigh.Z);

			outscat.X *= (float)Math.Exp(-scat.Y * CoefMie * AMie);
			outscat.Y *= (float)Math.Exp(-scat.Y * CoefMie * AMie);
			outscat.Z *= (float)Math.Exp(-scat.Y * CoefMie * AMie);

			outscat.X *= (float)Math.Exp(-scat.X * CoefOzone.X);
			outscat.Y *= (float)Math.Exp(-scat.X * CoefOzone.Y);
			outscat.Z *= (float)Math.Exp(-scat.X * CoefOzone.Z);

			return outscat;
		}
	}
}
