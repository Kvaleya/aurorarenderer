﻿using System;
using OpenTK;
using OpenTK.Input;

namespace AuroraRenderer
{
	// Parses keyboard and mouse input to generate first-person shooter style movement and mouse look
	public class FirstPersonInput
	{
		public bool InvertAxisLookX = false;
		public bool InvertAxisLookY = false;
		public bool InvertAxisMoveX = false;
		public bool InvertAxisMoveY = false;

		public Key KeyAxisMoveXPositive = Key.D;
		public Key KeyAxisMoveXNegative = Key.A;
		public Key KeyAxisMoveYPositive = Key.W;
		public Key KeyAxisMoveYNegative = Key.S;

		public float MouseSensitivity = 1.0f;

		Vector2 _cameraAngles = new Vector2(0, 0);

		public FirstPersonInput()
		{
		}

		public Vector2 GetMove(KeyboardInputState keyboard)
		{
			Vector2 move = Vector2.Zero;

			if(keyboard.IsKeyDown(KeyAxisMoveXPositive))
				move.X += 1;
			if(keyboard.IsKeyDown(KeyAxisMoveXNegative))
				move.X -= 1;
			if(keyboard.IsKeyDown(KeyAxisMoveYPositive))
				move.Y += 1;
			if(keyboard.IsKeyDown(KeyAxisMoveYNegative))
				move.Y -= 1;

			if(move.LengthSquared > 1)
				move.Normalize();

			if(InvertAxisMoveX)
				move.X = -move.X;
			if(InvertAxisMoveY)
				move.Y = -move.Y;

			return move;
		}

		Vector2 GetLook(MouseInputState mouse)
		{
			Vector2 look = mouse.MouseDelta;
			if(InvertAxisLookX)
				look.X = -look.X;
			if(InvertAxisLookY)
				look.Y = -look.Y;

			look *= MouseSensitivity;

			return look;
		}

		/// <summary>
		/// Returns camera yaw (0 .. 360) and pitch (-90 .. 90) in degress
		/// </summary>
		public Vector2 GetCameraAngles(MouseInputState mouse)
		{
			_cameraAngles += GetLook(mouse);

			while(_cameraAngles.X < 0)
				_cameraAngles.X += 360;
			while(_cameraAngles.X >= 360)
				_cameraAngles.X -= 360;
			if(_cameraAngles.Y > 90)
				_cameraAngles.Y = 90;
			if(_cameraAngles.Y < -90)
				_cameraAngles.Y = -90;

			return _cameraAngles;
		}
	}
}
