﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;

namespace AuroraRenderer
{
	/// <summary>
	/// Contains "game logic" and input parsing
	/// </summary>
	class WorldManager
	{
		Stopwatch _sw = new Stopwatch();

		EngineContext _engineContext;

		Vector3 _cameraPosition = new Vector3(0, 0, 18); // Set to a specific position so that an aurora curtain is in view
		FirstPersonInput _fpsInput = new FirstPersonInput();
		float _moveSpeed = 3;

		float _fov = 90;
		
		bool _wasFocused = false;

		public WorldManager(EngineContext engineContext)
		{
			_engineContext = engineContext;
			_engineContext.Ellapsed = 0;
			_sw.Start();
			_fpsInput.MouseSensitivity = 0.5f;
		}

		public void Update(bool focused)
		{
			// Update context
			var ellapsed = _sw.ElapsedTicks / (double)Stopwatch.Frequency;
			var deltaT = ellapsed - _engineContext.Ellapsed;
			_engineContext.Ellapsed = ellapsed;
			_engineContext.DeltaT = deltaT;

			if(focused)
			{
				_engineContext.Mouse.Update();
				_engineContext.Keyboard.Update();
			}
			if(focused && !_wasFocused)
			{
				_engineContext.Mouse.ClearState();
			}

			Vector2 move, look;

			// Camera movement
			move = _fpsInput.GetMove(_engineContext.Keyboard);
			look = _fpsInput.GetCameraAngles(_engineContext.Mouse) / 180 * (float)Math.PI;

			Quaternion rotation = Quaternion.FromAxisAngle(Vector3.UnitX, look.Y) * Quaternion.FromAxisAngle(Vector3.UnitY, look.X);
			var rotationMatrix = Matrix3.CreateFromQuaternion(rotation);

			Vector3 moveDelta = -rotationMatrix.Column2 * move.Y;
			moveDelta += rotationMatrix.Column0 * move.X;
			moveDelta *= _moveSpeed * (float)deltaT;

			moveDelta.Y = 0;

			if(_engineContext.Environment.EnableAuroraDebug)
			{
				moveDelta *= 4;
			}

			if(_engineContext.Keyboard.IsKeyDown(Key.LShift))
			{
				moveDelta *= 4;
			}

			if(_engineContext.Keyboard.IsKeyDown(Key.LAlt))
			{
				moveDelta /= 4;
			}

			_cameraPosition += moveDelta;

			// Pass resulting matrixes to renderer
			Matrix4 cameraMatrix = Matrix4.Identity;
			cameraMatrix.Row3 = new Vector4(-_cameraPosition, 1);
			cameraMatrix = cameraMatrix * new Matrix4(rotationMatrix);

			var projectionMatrix = Glob.Utils.GetProjectionPerspective(_engineContext.DisplayWidth, _engineContext.DisplayHeight, _fov, 0.125f, 16384f);

			_engineContext.Environment.CameraMatrix = cameraMatrix;
			_engineContext.Environment.ProjectionMatrix = projectionMatrix;
			_engineContext.Environment.CameraPosition = _cameraPosition;

			// Sunlight position input
			if(_engineContext.Keyboard.IsKeyDown(Key.E))
			{
				_engineContext.Environment.SunlightDirection = -rotationMatrix.Column2;
			}

			// Tonemapping toggle
			if(_engineContext.Keyboard.IsKeyPressed(Key.T))
			{
				_engineContext.Environment.EnableTonemap = !_engineContext.Environment.EnableTonemap;

				if(_engineContext.Environment.EnableTonemap)
				{
					_engineContext.TextOutput.Print(OutputType.Notify, "Tonemapping enabled");
				}
				else
				{
					_engineContext.TextOutput.Print(OutputType.Notify, "Tonemapping disabled");
				}
			}

			// Debug aurora view toggle
			if(_engineContext.Keyboard.IsKeyPressed(Key.R))
			{
				_engineContext.Environment.EnableAuroraDebug = !_engineContext.Environment.EnableAuroraDebug;

				if(_engineContext.Environment.EnableAuroraDebug)
				{
					_engineContext.TextOutput.Print(OutputType.Notify, "Aurora debug enabled");
				}
				else
				{
					_engineContext.TextOutput.Print(OutputType.Notify, "Aurora debug disabled");
				}
			}

			// Debug fluid sim view toggle
			if(_engineContext.Keyboard.IsKeyPressed(Key.V))
			{

				if(_engineContext.Environment.AuroraDebugLayerView == 65535)
				{
					_engineContext.Environment.AuroraDebugLayerView = 0;
				}
				else
				{
					_engineContext.Environment.AuroraDebugLayerView++;
				}
			}

			// Fov settings
			bool printFov = false;

			if(_engineContext.Keyboard.IsKeyPressed(Key.X))
			{
				_fov = _fov + 10;

				if(_fov > 150)
					_fov = 150;

				printFov = true;
			}

			if(_engineContext.Keyboard.IsKeyPressed(Key.C))
			{
				_fov = _fov - 10;

				if(_fov < 10)
					_fov = 10;

				printFov = true;
			}

			if(printFov)
			{
				_engineContext.TextOutput.Print(OutputType.Notify, "Current fov: " + _fov.ToString(CultureInfo.InvariantCulture) + " Change fov using X/C keys");
			}

			// Compute exposure from sun position
			_engineContext.Environment.Exposure = GetExposureFromSun(_engineContext.Environment.SunlightDirection.Y);

			_wasFocused = focused;
		}

		// Instead of calculating the exposure from the previous frame's histogram,
		// let's simply use the Y component of the sunlight vector to guess the
		// correct exposure, which is good enough for the purposes of this project
		float GetExposureFromSun(float sunVectorY)
		{
			float d = Math.Min(Math.Max((sunVectorY + 0.23f) / 0.25f, 0), 1); // 0 .. night, 1 .. day
			float nightPow = 1f;
			float dayPow = -8f;

			d = SmoothStep(0, 1, d);

			float pow = nightPow * (1 - d) + dayPow * d;

			return (float) Math.Pow(2, pow);
		}

		// https://en.wikipedia.org/wiki/Smoothstep
		float SmoothStep(float edge0, float edge1, float x)
		{
			// Scale, bias and saturate x to 0..1 range
			x = Math.Min(Math.Max((x - edge0) / (edge1 - edge0), 0.0f), 1.0f);
			// Evaluate polynomial
			return x * x * (3 - 2 * x);
		}
	}
}
