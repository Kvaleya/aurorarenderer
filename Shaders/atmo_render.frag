#version 430

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 outColor;

#include atmoScattering_antizem.glsl

layout(binding = 0) uniform sampler3D texLutScattering;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray10;
uniform vec4 ray11;

uniform vec3 sunDir;
uniform vec3 sunColor;
uniform float exposure;

uniform float cameraHeight;

vec3 GetRay(vec2 tc)
{
	return mix(
		mix(ray01.xyz, ray11.xyz, tc.x),
		mix(ray00.xyz, ray10.xyz, tc.x),
		tc.y);
}

void main() {
	vec3 ray = normalize(GetRay(screenPos));
	
	vec3 scattering = GetScattering(cameraHeight, ray, sunDir, sunColor, texLutScattering);
	
    outColor = vec4(scattering * exposure, 0.0);
}