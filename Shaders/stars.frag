#version 430

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 vTexCoord;
layout(location = 1) in vec3 vColor;

uniform float exposure;

void main() {
	vec2 toMiddle = (vec2(0.5) - vTexCoord) * 10;
	float dist = 1.0 - min(dot(toMiddle, toMiddle), 1.0);
	dist *= dist;
	dist *= dist;
	outColor.rgb = dist * vColor * exposure;
}