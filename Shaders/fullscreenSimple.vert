#version 430

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec2 screenPos;

vec2 positions[3] = vec2[3](
	vec2(-1.0, -1.0),
	vec2(3.0, -1.0),
	vec2(-1.0, 3.0)
);

vec2 texCoords[3] = vec2[3](
	vec2(0.0, 0.0),
	vec2(2.0, 0.0),
	vec2(0.0, 2.0)
);

void main() {
    gl_Position = vec4(positions[gl_VertexID], 0.0, 1.0);
	screenPos = texCoords[gl_VertexID];
}