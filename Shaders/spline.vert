#version 430

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec2 vertexTexCoord;

out gl_PerVertex {
    vec4 gl_Position;
};

uniform vec2 posOffset;
uniform vec2 posScale;

layout(location = 0) out vec2 vPos;
layout(location = 1) out vec2 vSplinePos;

void main() {
	vec2 pos = posOffset + vertexPosition.xy * posScale;
	vPos = pos;
	vSplinePos = vertexTexCoord.xy;
    gl_Position = vec4(pos * 2.0 - 1.0, 0.0, 1.0);
}