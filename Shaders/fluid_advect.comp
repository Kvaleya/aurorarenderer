#version 430

layout (binding = 0, rg16f) uniform restrict writeonly image2DArray texVelocityOut;
layout (binding = 1, rg16f) uniform restrict writeonly image2DArray texQuantityOut;

layout(binding = 0) uniform sampler2DArray texVelocityIn;
layout(binding = 1) uniform sampler2DArray texQuantityIn;

uniform float deltaT;

uniform vec2 gridSizeRcp;
uniform float gridSpacingRcp;

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main()
{
	ivec3 ipos = ivec3(gl_GlobalInvocationID.xyz);
	
	vec2 pos = (ipos.xy + 0.5) * gridSizeRcp;
	pos -= deltaT * gridSpacingRcp * gridSizeRcp * texture(texVelocityIn, vec3(pos, ipos.z)).xy;
	
	imageStore(texVelocityOut, ipos, texture(texVelocityIn, vec3(pos, ipos.z)));
	imageStore(texQuantityOut, ipos, texture(texQuantityIn, vec3(pos, ipos.z)));
}