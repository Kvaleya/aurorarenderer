float maxcomp(vec2 v)
{
	return max(v.x, v.y);
}

float maxcomp(vec3 v)
{
	return max(v.x, max(v.y, v.z));
}

float maxcomp(vec4 v)
{
	return max(v.x, max(v.y, max(v.z, v.w)));
}

float mincomp(vec2 v)
{
	return min(v.x, v.y);
}

float mincomp(vec3 v)
{
	return min(v.x, min(v.y, v.z));
}

float mincomp(vec4 v)
{
	return min(v.x, min(v.y, min(v.z, v.w)));
}

// https://stackoverflow.com/questions/39393560/glsl-memorybarriershared-usefulness
#define MEMORY_BARRIER_GROUPSHARED memoryBarrierShared(); barrier();

#define DEPTH_NEAR 100000000000.0
#define DEPTH_FAR 0.0

#define KV_PI 3.1415926535897932384626433832795