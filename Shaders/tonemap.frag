#version 430

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texScene;

float Luma(vec3 color)
{
	return dot(color, vec3(0.299, 0.587, 0.114));
}

// Frostbite gamma correction
vec3 AccurateLinearToSRGB(vec3 linearCol)
{
	vec3 sRGBLo = linearCol * 12.92;
	vec3 sRGBHi = (pow(abs(linearCol), 1.0 / vec3(2.4)) * 1.055)  - 0.055;
	vec3 sRGB = (max(max(linearCol.x, linearCol.y), linearCol.z) <= 0.0031308) ? sRGBLo : sRGBHi;
	return sRGB;
}

vec3 ToneMapFilmic_Hejl2015(vec3 hdr)
{
	vec4 vh = vec4(hdr, 1.0);
	vec4 va = (1.425 * vh) + 0.05;
	vec4 vf = ((vh * va + 0.004) / ((vh * (va + 0.55) + 0.0491)));
	vf = vf - 0.0821;
	return vf.rgb / vf.www;
}

layout(location = 0) in vec2 screenPos;

void main()
{
	// Exposure is pre-applied to the color in light buffer
	vec4 color = texture(texScene, screenPos);
	
	#ifndef NO_TONEMAP_GAMMA
	color.rgb = ToneMapFilmic_Hejl2015(color.rgb);

	color.rgb = AccurateLinearToSRGB(color.rgb);
	#endif
	
	outColor = vec4(color.rgb, 1.0);
}