#version 430

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 outColor;

#ifdef ARRAY
layout(binding = 0) uniform sampler2DArray tex;
#else
layout(binding = 0) uniform sampler2D tex;
#endif

uniform float layer;
uniform float aspect;
uniform float scale;

void main() {	
	vec3 tc = vec3(screenPos / scale, layer);
	tc.y *= aspect;
	
	if(aspect < 1.0)
		tc /= aspect;
	
	if(tc.x < 0.0 || tc.y < 0.0 || tc.x > 1.0 || tc.y > 1.0)
		discard;
	#ifdef ARRAY
    outColor = texture(tex, tc.xyz);
	#else
    outColor = texture(tex, tc.xy);
	#endif
}